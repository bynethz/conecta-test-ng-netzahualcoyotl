import { TestBed } from '@angular/core/testing';

import { BankappService } from './bankapp.service';

describe('BankappService', () => {
  let service: BankappService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BankappService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
