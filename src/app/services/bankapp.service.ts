import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BankappService {
  url = {
    account: 'http://bankapp.endcom.mx/api/bankappTest/cuenta',
    balances: 'http://bankapp.endcom.mx/api/bankappTest/saldos',
    cards: 'http://bankapp.endcom.mx/api/bankappTest/tarjetas',
    transactions: 'http://bankapp.endcom.mx/api/bankappTest/movimientos'
  }

  constructor(
    private _http: HttpClient
  ) { }

  getAccount() : Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                  .set('Accept', 'application/json');

    return this._http.get(`${this.url.account}`, {headers: headers})
  }

  getBalances() : Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                  .set('Accept', 'application/json');

    return this._http.get(`${this.url.balances}`, {headers: headers})
  }

  getCards() : Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                  .set('Accept', 'application/json');

    return this._http.get(`${this.url.cards}`, {headers: headers})
  }

  getTransactions() : Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                  .set('Accept', 'application/json');

    return this._http.get(`${this.url.transactions}`, {headers: headers})
  }
}
