export type Status = 'activa' | 'desactivada';

export interface IAccount {
  cuenta: number,
  nombre: string,
  ultimaSesion: string,
  id: number
}

export interface IBalance {
  cuenta: number,
  saldoGeneral: number,
  ingresos: number,
  gastos: number,
  id: number
}

export interface ITransaction {
  fecha: string,
  descripcion: string,
  monto: string,
  tipo: string,
  id: number
}

export interface ICard {
  tarjeta: string,
  nombre: string,
  saldo: number,
  estado: Status,
  tipo: string,
  id: number
}
