import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'balance-card',
  templateUrl: './balance-card.component.html',
  styleUrls: ['./balance-card.component.scss']
})
export class BalanceCardComponent implements OnInit {
  @Input() title: string = '';
  @Input() isGeneral: boolean = false;
  @Input() amount: number = 0

  constructor() { }

  ngOnInit(): void {
  }

}
