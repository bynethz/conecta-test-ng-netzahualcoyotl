import { Component, Input, OnInit } from '@angular/core';

type StyleLogo = 'green' | 'grey';

@Component({
  selector: 'logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {

  @Input() styleLogo: string = 'green';

  constructor() { }

  ngOnInit(): void {
  }

}
