import { Component, Input, OnInit } from '@angular/core';
import { ICard, Status } from 'src/app/interfaces/data.interfaces';
import Utils from 'src/app/utils/utils';

@Component({
  selector: 'credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss']
})
export class CreditCardComponent implements OnInit {
  public utils = new Utils();
  @Input() card: ICard;

  constructor() {
    this.card = {
      id: 0,
      estado: 'activa',
      nombre: '',
      saldo: 0,
      tarjeta: '',
      tipo: ''
    }
  }

  ngOnInit(): void {
  }

}
