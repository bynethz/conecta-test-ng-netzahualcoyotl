import { Component, OnInit } from '@angular/core';
import { IBalance } from 'src/app/interfaces/data.interfaces';
import { BankappService } from 'src/app/services/bankapp.service';

@Component({
  selector: 'app-balances',
  templateUrl: './balances.component.html',
  styleUrls: ['./balances.component.scss']
})
export class BalancesComponent implements OnInit {

  public balances: IBalance;

  constructor(
    private _bankappService: BankappService
  ) {
    this.balances = {
      cuenta: 0,
      gastos: 0,
      id: 0,
      ingresos: 0,
      saldoGeneral: 0
    }
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this._bankappService.getBalances().subscribe(
      response => {
        this.balances = response.saldos[0];
      },
      error => {
        console.log(error)
      }
    )
  }

}
