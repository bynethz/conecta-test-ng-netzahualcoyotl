import { Component, OnInit } from '@angular/core';
import { ITransaction } from 'src/app/interfaces/data.interfaces';
import { BankappService } from 'src/app/services/bankapp.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  public transactions: ITransaction[] = [];

  constructor(
    private _bankappService: BankappService
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this._bankappService.getTransactions().subscribe(
      response => {
        this.transactions = response.movimientos;
      },
      error => {
        console.log(error)
      }
    )
  }

}
