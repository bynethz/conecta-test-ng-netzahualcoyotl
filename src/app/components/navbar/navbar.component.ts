import { Component, OnInit } from '@angular/core';
import { IAccount } from 'src/app/interfaces/data.interfaces';
import { BankappService } from 'src/app/services/bankapp.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public account: IAccount;

  constructor(
    private _bankappService: BankappService
  ) {
    this.account = {
      cuenta: 0,
      nombre: '',
      ultimaSesion: '',
      id: 0
    }
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this._bankappService.getAccount().subscribe(
      response => {
        this.account = response.cuenta[0]
      },
      error => {
        console.log(error)
      }
    )
  }

}
