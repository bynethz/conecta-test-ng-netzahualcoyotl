import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ICard } from 'src/app/interfaces/data.interfaces';
import { BankappService } from 'src/app/services/bankapp.service';
import Utils from 'src/app/utils/utils';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss'],
})
export class CardsComponent implements OnInit {
  public utils = new Utils();
  closeResult = '';
  public cards: ICard[] = [];
  public dataCard = {
    account: 0,
    amount: '',
    cardNumber: '',
    issuer: '',
    marca: '',
    name: '',
    status: '',
    type: '',
  };
  public form = new FormGroup({
    cardNumber: new FormControl('', Validators.minLength(16)),
    account: new FormControl('', Validators.required),
    issuer: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    marca: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
  });

  constructor(
    private modalService: NgbModal,
    private _bankappService: BankappService
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this._bankappService.getCards().subscribe(
      (response) => {
        this.cards = response.tarjetas;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  open(content: TemplateRef<any>) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = ``;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  save() {
    this.modalService.dismissAll();
    this.dataCard.cardNumber = this.utils.cc_format(this.form.value.cardNumber + '');
    this.dataCard.account = this.form.value.account;
    this.dataCard.issuer = this.form.value.issuer;
    this.dataCard.name = this.form.value.name;
    this.dataCard.marca = this.form.value.marca;
    this.dataCard.status = this.form.value.status;
    this.dataCard.amount = this.form.value.amount;
    this.dataCard.type = this.form.value.type;
    console.log(this.dataCard);
    alert( JSON.stringify(this.dataCard) );

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
