import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BalancesComponent } from './components/balances/balances.component';
import { CardsComponent } from './components/cards/cards.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { LogoComponent } from './shared/logo/logo.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { BalanceCardComponent } from './shared/balance-card/balance-card.component';
import { CreditCardComponent } from './shared/credit-card/credit-card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './components/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BalancesComponent,
    CardsComponent,
    TransactionsComponent,
    LogoComponent,
    PrincipalComponent,
    BalanceCardComponent,
    CreditCardComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
