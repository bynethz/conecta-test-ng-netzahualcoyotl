# BankApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Types for project / Interfaces para el proyecto

type Status = 'activa' | 'desactivada';

interface IAccount:
- cuenta: number,
- nombre: string,
- ultimaSesion: string,
- id: number

interface IBalance:
- cuenta: number,
- saldoGeneral: number,
- ingresos: number,
- gastos: number,
- id: number

interface ITransaction:
- fecha: string,
- descripcion: string,
- monto: string,
- tipo: string,
- id: number

interface ICard:
- tarjeta: string,
- nombre: string,
- saldo: number,
- estado: Status,
- tipo: string,
- id: number

## Structure / Estructura

Components:
- balances
- cards
- footer
- navbar
- principal
- transactions

Interfaces

Services
- BankApp Service

Shared
- Balance Card
- Credit Card
- Logo

Utils

## Running unit tests


## Running end-to-end tests


## Further help

